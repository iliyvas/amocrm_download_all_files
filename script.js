define(['jquery'], function($){
  var DownloadAllFilesWidget = function () {
    var self = this;
    this.ajaxComplete = true;
    this.callbacks = {
      render: function(){
        return true;
      },
      init: function(){
        return true;
      },
      bind_actions: function(){
        $(document).off('ajaxComplete.downloadAllFiles')
                   .on('ajaxComplete.downloadAllFiles', function() {
          self.ajaxComplete = true;
        });
        $('body').off('modal:loaded.downloadAllFiles')
                 .on('modal:loaded.downloadAllFiles', function(e) {
          $target = $(e.target);
          // Check if button has been already inserted
          if ($target.find('.thread_letter__download-all').length) {
            return true;
          }
          if ($target.find('.mail_thread__wrapper').length) {
            $target.find('.thread_letter__attachments').each(function(i, el) {
              $attachments = $(el);
              $attachments_containers = $attachments.find('.thread_letter__attachment-container ');
              $button = $('<div class="thread_letter__attachment-container thread_letter__download-all">' +
                          '<a href="#" class="thread_letter__attachment" style="border: 1px solid #DBDEE2;border-radius:3px;padding: 9px 20px;">' +
                            '<span class="thread_letter__attachment-info">' +
                              '<span class="thread_letter__attachment-name">Скачать все</span>' +
                            '</span>' +
                          '</a>' +
                        '</div>');
              $attachments.prepend($button);
              $button.click(function(e) {
                e.stopPropagation();
                var $this = $(this);
                var $links = $this.closest('.thread_letter__attachments')
                                  .find('.thread_letter__attachment');
                var i = 1;
                var interval = setInterval(function() {
                  if (self.ajaxComplete) {
                    self.ajaxComplete = false;
                    $links.eq(i).click();
                    i++;
                    if ($links.length == i) {
                      clearInterval(interval);
                    }
                  }
                }, 700);
              });
            });
          }
        });
        return true;
      },
      settings: function(){
        $('input[name="dummy"]').val('dummy');
        $('input[name="dummy"]').trigger('change');
        return true;
      },
      onSave: function(){
        return true;
      },
      destroy: function(){
      },
      contacts: {
          selected: function(){
          }
        },
      leads: {
          selected: function(){
          }
        },
      tasks: {
          selected: function(){
          }
        }
    };
    return this;
  };

return DownloadAllFilesWidget;
});